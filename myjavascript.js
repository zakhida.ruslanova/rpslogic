const PAPER = 'PAPER';
const SCISSORS = 'SCISSORS';
const ROCK = 'ROCK';

const MOVES = [PAPER, SCISSORS, ROCK];

const TIE = 'TIE';
const WIN = 'WIN';
const LOSE = 'LOSE';

var playerScore = 0;
var computerScore = 0;

function execute(playerMove) {
    let computerMove = calculateComputerChoice();
    showComputerChoice(computerMove);
    let gameResult = calculateWinner(playerMove, computerMove);
    showGameResult(gameResult);
    updateGameScore(gameResult);
}

function calculateComputerChoice() {
    let randomValue = Math.floor(Math.random() * 3);
    return MOVES[randomValue];
}

function calculateWinner(playerMove, computerMove) {
    if (playerMove === computerMove)
        return TIE;

    switch (playerMove) {
        case ROCK: return computerMove === SCISSORS ? WIN : LOSE;
        case PAPER: return computerMove === ROCK ? WIN : LOSE;
        case SCISSORS: return  computerMove === PAPER ? WIN : LOSE;
    }
    throw new Error('Unknown playerMove',playerMove);
}

function updateGameScore(gameResult) {
    if (gameResult === WIN) {
        playerScore++;
        updatePlayerWins(playerScore);
    }
    if (gameResult === LOSE) {



        computerScore++;
        updateComputerWins(computerScore);
    }
}

/* ------- Update User Interface (HTML) -------------- */

function showComputerChoice(computerMove) {
    let computerChoiceElement = document.getElementById('computerChoice');
    computerChoiceElement.value = computerMove;
}

function showGameResult(gameResult) {
    let gameResultElement = document.getElementById('gameResult');
    gameResultElement.value = gameResult;
}

function updatePlayerWins(playerScore) {
    let playerWinCountsElement = document.getElementById('playerWinCounts');
    playerWinCountsElement.value = playerScore;
}

function updateComputerWins(computerScore) {
    let computerWinCountsElement = document.getElementById('computerWinCounts');
    computerWinCountsElement.value = computerScore;
}
